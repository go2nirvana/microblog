# Welcome to Wellfound's microblog assessment!

## Goal
Your task is to update the home page of this Flask app to display all of the rows in the blog posts table instead of just the static single row.

Clone/fork this repository and make a commit with this change. After you've made your changes, please take a screenshot of the updated home page and include it in the README of your public repository. Finally, email the link to your public repository to clayton.bridge@wellfound.co, and please include your full name in the email!

<BR><BR>

## Contributions
Feel free to clone this repo locally and use your own editor of choice.

If you would rather use the Gitpod development environment for this app:

- Change the dropdown that says "Web IDE" to "Gitpod" (if it already says "Gitpod" skip this step)
- Click the button that says "Gitpod"


## Changes made

Added list view for posts:
![img.png](img.png)

Added placeholder is there is no posts:
![img_1.png](img_1.png)

Refactored Dockerfile:
- Removed virtualenv setup as it contradicts the goal for using containers.

Optimized requirements.txt:
- Removed packages that are installed as dependencies.

Added missing translations

General code improvements:
- Optimized imports
- Optimized `is None`/`is not None` checks
- Refactored multiline statements
- Reduced code nesting depth
- General code improvements

Note: code refactoring is intentionally light to preserve current styling approach, but to address severe styling 
derivations at the same time
